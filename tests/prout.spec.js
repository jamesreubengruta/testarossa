const { test, expect } = require("@playwright/test");

test("Dashboard", async ({ page }) => {
  const base_url = "https://torontodough.one/";

  await page.goto(base_url);

  const pageTitle = page.title();
  console.log("The title is: ", pageTitle);

  await expect(page).toHaveTitle(/Prout/);
  const url = page.url();
  console.log("PAGE URL IS: ", url);
  await expect(page).toHaveURL(base_url + "intro");

  await page.click('[data-testid="ArrowForwardIcon"]');

  await expect(page).toHaveURL(base_url + "login");

  await page.fill('input[type="email"]', "jamesreubengruta@gmail.com");

  await page.fill('input[type="password"]', "Qwerty123!");

  await page.click('button[type="submit"]');

  await expect(page).toHaveURL(base_url);

  const healthCareVisibility = page.locator(
    'p.MuiTypography-root:has-text("Healthcare")'
  );

  const legalVisibility = page.locator(
    'p.MuiTypography-root:has-text("Legal")'
  );

  const jobVisibility = page.locator('p.MuiTypography-root:has-text("Jobs")');

  const housingVisibility = page.locator(
    'p.MuiTypography-root:has-text("Housing")'
  );

  await expect(healthCareVisibility).toBeVisible();
  await expect(legalVisibility).toBeVisible();
  await expect(jobVisibility).toBeVisible();
  await expect(housingVisibility).toBeVisible();

  const collection = page.$$('a[class="navlink-black"]');

  console.log((await collection).length);

  expect((await collection).length).toBeGreaterThanOrEqual(10);

  const items = await page.$$("//div[@class='p-4 col']//p[1]");

  console.log("items count", items.length);

  for (const i of items) {
    //expect(i).toBeVisible();
    //expect(items[i].innerText()).not.toBe("");
    console.log("item", await i.innerText());
    expect(await i.innerText()).not.toBe("");
  }

  page.close();
});
