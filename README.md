#Basic testing using Playwright

**Playwright** is a Node.js library to automate _Chromium_, _Firefox_, and _WebKit_ with a single API. Playwright is built to enable cross-browser web automation that is evergreen, capable, reliable, and fast.

Installing playwright:

```abap
To install:
- Via VSCODE: search and install Playwright Test for Vscode from marketplace.
once installed, go to view -> command palette, then enter install command.

- Via terminal: npm init playwright@latest;
```

running playwright:

```jsx
//running all tests
npx playwright test

//running specific test file only
npx playwright test test.spec.js

//running with specific browser only
npx playwright... --project=chromium

//running with headed version
npx playwright... --headed

//runnin with debugger
npx playwright... --debug
```

Useful keywords:

```jsx
test("name of test here", async ({ page }) => {
  // to be used here
});

//to go to a specific page
await page.goto("https://urlofwebsite.com");

//get page title
page.title();

//check if page has specific title
expect(page).toHaveTitle(/sometextHere/);

//check if page currently has specific URL
expect(page).toHaveURL("https://anyurlhere/something");

//fill out textfields
page.fill("selector here", "value here");

//close the page
page.close();
```

Ways to get elements:

```jsx
//property
const element = await page.$('text=Healthcare');
// Selects an element with the exact text content "Healthcare"

//css
const element = await page.$('p.MuiTypography-root');

//xpath
const element = await page.$('xpath=//p[@class="MuiTypography-root"]');
// Equivalent to the previous example, but using XPath

/* xpath explanation

//: This denotes selecting elements from anywhere in the document, not just at the root. It's like a wildcard for the hierarchy. So, this expression will search through the entire document to find matches.

p: This is the element type we're looking for. In this case, we're looking for all <p> (paragraph) elements.

[@class="MuiTypography-root"]: This is a predicate, a condition that must be met for the element to be selected. Specifically:

@class: The @ symbol denotes an attribute of the element. In this case, we're looking at the class attribute of the <p> elements.

="MuiTypography-root": This specifies the value that the attribute must have for the element to be selected. So we're looking for <p> elements where the class attribute has a value of "MuiTypography-root".
*/

using page.$$ instead of page.$
```

references:
https://wary-shield-e2a.notion.site/Automation-23b4ec838a704a14a8e724f1d30cd258?pvs=4
